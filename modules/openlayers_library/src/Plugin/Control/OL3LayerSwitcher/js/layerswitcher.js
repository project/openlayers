Drupal.openlayers.pluginManager.register({
  fs: 'openlayers.Control:OL3LayerSwitcher',
  init: function(data) {
    return new OL3LayerSwitcher({
      tipLabel: 'Légende' // Optional label for button
    });
  }
});
