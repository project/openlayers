/**
 * OpenLayers 3 Layer Switcher Control.
 * See [the examples](./examples) for usage.
 * @constructor
 * @extends {ol.control.Control}
 * @param {Object} opt_options Control options, extends olx.control.ControlOptions adding:
 *                              **`tipLabel`** `String` - the button tooltip.
 */
class OL3LayerSwitcher extends ol.control.Control {

  /**
   * @param {Object} [opt_options] Control options.
   */
  constructor(opt_options) {
    const options = opt_options || {};

    const tipLabel = options.tipLabel ?
      options.tipLabel : 'Legend';

    const mapListeners = [];

    const hiddenClassName = 'ol-unselectable ol-control layer-switcher';
    const shownClassName = hiddenClassName + ' shown';

    const element = document.createElement('div');
    element.className = hiddenClassName;

    const button = document.createElement('button');
    button.setAttribute('title', tipLabel);
    element.appendChild(button);

    const panel = document.createElement('div');
    panel.className = 'panel';
    element.appendChild(panel);

    super({
      element: element,
      target: options.target,
    });

    this.hiddenClassName = hiddenClassName;
    this.shownClassName = shownClassName;
    this.panel = panel;

    var this_ = this;
    
    element.onmouseover = function(e) {
      this_.showPanel();
    };

    button.onclick = function(e) {
      this_.showPanel();
      e.preventDefault();
    };

    element.onmouseout = function(e) {
      e = e || window.event;
      if (!element.contains(e.toElement)) {
        this_.hidePanel();
      }
    };
  }

  /**
   * Show the layer panel.
   */
  showPanel() {
    if (this.element.className != this.shownClassName) {
      this.element.className = this.shownClassName;
      this.renderPanel();
    }
  }

  /**
   * Hide the layer panel.
   */
  hidePanel() {
    if (this.element.className != this.hiddenClassName) {
      this.element.className = this.hiddenClassName;
    }
  };

  /**
   * Re-draw the layer panel to represent the current state of the layers.
   */
  renderPanel() {
    this.ensureTopVisibleBaseLayerShown_();

    while(this.panel.firstChild) {
      this.panel.removeChild(this.panel.firstChild);
    }

    var ul = document.createElement('ul');

    this.panel.appendChild(ul);
    this.renderLayers_(this.getMap(), ul);
  };

  /**
   * Set the map instance the control is associated with.
   * @param {ol.Map} map The map instance.
   */
/*
  setMap(map) {
    // Clean up listeners associated with the previous map
    for (var i = 0, key; i < this.mapListeners.length; i++) {
      this.getMap().unByKey(this.mapListeners[i]);
    }
    this.mapListeners.length = 0;
    // Wire up listeners etc. and store reference to new map
    ol.control.Control.prototype.setMap.call(this, map);
    if (map) {
      var this_ = this;
      this.mapListeners.push(map.on('pointerdown', function() {
        this_.hidePanel();
      }));
      this.renderPanel();
    }
  };
*/

  /**
   * Ensure only the top-most base layer is visible if more than one is visible.
   * @private
   */
  ensureTopVisibleBaseLayerShown_() {
    var lastVisibleBaseLyr;
    this.forEachRecursive(this.getMap(), function(l, idx, a) {
      if (l.get('type') === 'base' && l.getVisible()) {
        lastVisibleBaseLyr = l;
      }
    });
    if (lastVisibleBaseLyr) this.setVisible_(lastVisibleBaseLyr, true);
  };

  /**
   * Toggle the visible state of a layer.
   * Takes care of hiding other layers in the same exclusive group if the layer
   * is toggle to visible.
   * @private
   * @param {ol.layer.Base} The layer whos visibility will be toggled.
   */
  setVisible_(lyr, visible) {
    var map = this.getMap();
    lyr.setVisible(visible);
    if (visible && lyr.get('type') === 'base') {
      // Hide all other base layers regardless of grouping
      this.forEachRecursive(map, function(l, idx, a) {
        if (l != lyr && l.get('type') === 'base') {
          l.setVisible(false);
        }
      });
    }
  };

  /**
   * Render all layers that are children of a group.
   * @private
   * @param {ol.layer.Base} lyr Layer to be rendered (should have a title property).
   * @param {Number} idx Position in parent group list.
   */
  renderLayer_(lyr, idx) {

    var this_ = this;

    var li = document.createElement('li');

    var lyrTitle = lyr.get('title');
    var lyrId = lyr.get('title').replace(/\s+/g, '-') + '_' + idx;

    var label = document.createElement('label');

    if (lyr.getLayers) {

      li.className = 'group';
      label.innerHTML = lyrTitle;
      li.appendChild(label);
      var ul = document.createElement('ul');
      li.appendChild(ul);

      this.renderLayers_(lyr, ul);

    } else {

      var input = document.createElement('input');
      if (lyr.get('type') === 'base') {
        input.type = 'radio';
        input.name = 'base';
      } else {
        input.type = 'checkbox';
      }
      input.id = lyrId;
      input.checked = lyr.get('visible');
      input.onchange = function(e) {
        this_.setVisible_(lyr, e.target.checked);
      };
      li.appendChild(input);

      label.htmlFor = lyrId;
      label.innerHTML = lyrTitle;
      li.appendChild(label);

    }

    return li;

  };

  /**
   * Render all layers that are children of a group.
   * @private
   * @param {ol.layer.Group} lyr Group layer whose children will be rendered.
   * @param {Element} elm DOM element that children will be appended to.
   */
  renderLayers_(lyr, elm) {
    var lyrs = lyr.getLayers().getArray().slice().reverse();
    for (var i = 0, l; i < lyrs.length; i++) {
      l = lyrs[i];
      if (l.get('title') == undefined) {
        l.set('title', 'Unknown layer - ' + i);
      }
      if (l.get('title')) {
        elm.appendChild(this.renderLayer_(l, i));
      }
    }
  };

  /**
   * **Static** Call the supplied function for each layer in the passed layer group
   * recursing nested groups.
   * @param {ol.layer.Group} lyr The layer group to start iterating from.
   * @param {Function} fn Callback which will be called for each `ol.layer.Base`
   * found under `lyr`. The signature for `fn` is the same as `ol.Collection#forEach`
   */
  forEachRecursive(lyr, fn) {
    lyr.getLayers().forEach(function(lyr, idx, a) {
      fn(lyr, idx, a);
      if (lyr.getLayers) {
        this.forEachRecursive(lyr, fn);
      }
    });
  };

}