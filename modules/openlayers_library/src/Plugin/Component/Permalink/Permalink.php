<?php

namespace Drupal\openlayers_library\Plugin\Component\Permalink;

use Drupal\openlayers\Types\Component;

/**
 * FIX - insert comment here.
 *
 * @OpenlayersPlugin(
 *   id = "Permalink",
 *   description = "The HTML 5 History API is used to update the browser
 *     URL with the current zoom-level, center and rotation when the map
 *     is moved."
 * )
 */
class Permalink extends Component {

}
