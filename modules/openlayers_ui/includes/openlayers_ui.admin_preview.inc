<?php

/**
 * @file
 * This file holds the functions for the Openlayers preview page.
 *
 * @ingroup openlayers
 */

use Drupal\openlayers\Openlayers;
use Drupal\openlayers\Config;

/**
 * Menu callback; displays the openlayers module preview page.
 *
 * @see system_settings_form()
 */
function openlayers_ui_admin_preview($form, &$form_state) {

  $form['map_preview'] = array(
    '#type' => 'fieldset',
    '#title' => 'Map Preview',
  );

  //  Temporary fix while we find out why preview page gets stuck on
  //  'openlayers_examples_map_blocklayerswitcher'.
  if (Config::get('openlayers.preview') == 'openlayers_examples_map_blocklayerswitcher') {
    Config::set('openlayers.preview', 'openlayers_map_ui_default');
  }

  $current_preview = ($preview = Config::get('openlayers.preview')) ? $preview : 'openlayers_map_ui_default';

  $options = array('' => t('- Select the map to preview -'));
  $maps = array();
  foreach (Openlayers::loadAllExportable('Map') as $machine_name => $data) {
    if (!is_object($data) || (property_exists($data, 'disabled') && ($data->disabled == 1 || $data->disabled == TRUE))) {
      continue;
    }
    $options[$machine_name] = $data->name;
  }
  
  $form['map_preview']['preview'] = array(
    '#type' => 'select',
    '#title' => 'Select the Openlayers map you wish to preview.',
    '#options' => $options,
    '#default_value' => $current_preview,
  );

  if (array_key_exists($current_preview, $options)) {
    $map = Openlayers::load('map', $current_preview);

    $form['map_preview']['map'] = array(
      '#type' => 'openlayers',
      '#map' => $map,
    );

    //  If the map height is specified as relative, force the previewed map height to 400px to ensure that a map is displayed 
    if ($form['map_preview']['map']['#map']->getOption('height') && substr($form['map_preview']['map']['#map']->getOption('height'), -1) == '%') {
      $form['map_preview']['map']['#map']->setOption('height', '400px');
      drupal_set_message('Map height set to 400px for preview purposes only, as original map height was set to a non-absolute value.', 'warning');
    }
  }

  $form['buttons'] = array(
    'submit' => array(
      '#type' => 'submit',
      '#value' => 'Select map',
    ),
  );

  return $form;
}

/**
 * Submit callback of the Openlayers settings page.
 */
function openlayers_ui_admin_preview_submit($form, &$form_state) {
  $values = $form_state['values'];

  if (isset($values['preview'])) {
    Config::set('openlayers.preview', $values['preview']);
  }
  else {
    Config::clear('openlayers.preview');
  }

  drupal_set_message(t('The map selection has been saved.'));
  drupal_flush_all_caches();
}
