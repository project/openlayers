<?php

namespace OpenlayersSymfony\Component\Yaml\Exception;

/**
 * Exception class thrown when an error occurs during parsing.
 *
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @author Romain Neutron <imprec@gmail.com>
 *
 * @api
 */
class RuntimeException extends \RuntimeException implements ExceptionInterface {

}
