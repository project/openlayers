<?php

namespace OpenlayersDrupal\Component\Plugin\Exception;

/**
 * FIX - insert comment here.
 *
 * Generic Plugin exception class to be thrown when no more specific class
 * is applicable.
 */
class PluginException extends \Exception implements ExceptionInterface {

}
