<?php

namespace OpenlayersDrupal\Component\Plugin\Exception;

/**
 * FIX - insert comment here.
 *
 * Extended interface for exceptions thrown specifically by the Mapper subsystem
 * within the Plugin component.
 */
interface MapperExceptionInterface extends ExceptionInterface {

}
