<?php

namespace Drupal\openlayers\Plugin\Control\ScaleLine;

use Drupal\openlayers\Types\Control;

/**
 * FIX - Insert short comment here.
 *
 * @OpenlayersPlugin(
 *  id = "ScaleLine",
 *  description = "A control displaying rough x-axis distances, calculated
 *    for the center of the viewport."
 * )
 */
class ScaleLine extends Control {

}
