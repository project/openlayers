<?php

namespace Drupal\openlayers\Plugin\Control\Zoom;

use Drupal\openlayers\Types\Control;

/**
 * FIX - Insert short comment here.
 *
 * @OpenlayersPlugin(
 *  id = "Zoom",
 *  description = "Provides a button, one for zoom in and one for zoom out."
 * )
 */
class Zoom extends Control {

}
