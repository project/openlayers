<?php

namespace Drupal\openlayers\Plugin\Control\OverviewMap;

use Drupal\openlayers\Types\Control;

/**
 * FIX - Insert short comment here.
 *
 * @OpenlayersPlugin(
 *  id = "OverviewMap",
 *  description = "Create a new control with a map acting as an overview map
 *    for an other defined map."
 * )
 */
class OverviewMap extends Control {

}
