<?php

namespace Drupal\openlayers\Plugin\Control\Rotate;

use Drupal\openlayers\Types\Control;

/**
 * FIX - Insert short comment here.
 *
 * @OpenlayersPlugin(
 *  id = "Rotate",
 *  description = "Provides a button control to reset rotation to 0."
 * )
 */
class Rotate extends Control {

}
