<?php

namespace Drupal\openlayers\Plugin\Control\ZoomToExtent;

use Drupal\openlayers\Types\Control;

/**
 * FIX - Insert short comment here.
 *
 * @OpenlayersPlugin(
 *  id = "ZoomToExtent",
 *  description = "A button control which changes the map view to a specific
 *    extent."
 * )
 */
class ZoomToExtent extends Control {

}
