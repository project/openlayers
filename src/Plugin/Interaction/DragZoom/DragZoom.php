<?php

namespace Drupal\openlayers\Plugin\Interaction\DragZoom;

use Drupal\openlayers\Types\Interaction;

/**
 * FIX - Insert short comment here.
 *
 * @OpenlayersPlugin(
 *  id = "DragZoom",
 *  description =
 *    "Allows the user to zoom the map by clicking and dragging on the map
 *    when the [ALT] and [SHIFT] keys are held down."
 * )
 */
class DragZoom extends Interaction {

}
