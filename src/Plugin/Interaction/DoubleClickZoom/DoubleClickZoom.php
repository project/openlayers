<?php

namespace Drupal\openlayers\Plugin\Interaction\DoubleClickZoom;

use Drupal\openlayers\Types\Interaction;

/**
 * FIX - Insert short comment here.
 *
 * @OpenlayersPlugin(
 *  id = "DoubleClickZoom",
 *  description = "Allows the user to zoom by double-clicking on the map."
 * )
 */
class DoubleClickZoom extends Interaction {

}
