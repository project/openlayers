<?php

namespace Drupal\openlayers\Plugin\Interaction\MouseWheelZoom;

use Drupal\openlayers\Types\Interaction;

/**
 * FIX - Insert short comment here.
 *
 * @OpenlayersPlugin(
 *  id = "MouseWheelZoom",
 *  description =
 *    "Allows the user to zoom the map by scrolling the mouse wheel."
 * )
 */
class MouseWheelZoom extends Interaction {

}
