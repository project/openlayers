Drupal.openlayers.pluginManager.register({
  fs: 'openlayers.Style:Circle',
  init: function(data) {
    return function (feature, resolution) {
      if (!(feature instanceof ol.Feature)) {
        return null;
      }
      var geometry = feature.getGeometry().getType();
      var geometry_style = data.opt[geometry] || data.opt['default'];

      var strokeColor = geometry_style.stroke.color || '';
      var strokeWidth = geometry_style.stroke.width + ' ' || '';
      var strokeDash = geometry_style.stroke.lineDash || '';
      var fillColor = geometry_style.fill.color || '';

      var imageRadius = geometry_style.image.radius + ' ' || '';
      var imageFillColor = geometry_style.image.fill.color || '';
      var imageStrokeColor = geometry_style.image.stroke.color || '';
      var imageStrokeWidth = geometry_style.image.stroke.width + ' ' || '';
      var imageStrokeDash = geometry_style.image.stroke.lineDash || '';

      //  Replace tokens in fields
      if (feature) {
        var featureProperties = feature.getProperties();
        
        for (key in featureProperties) {
          if (key != 'popup_content' && key != 'tooltip_content') {
            strokeColor = strokeColor.replace('${' + key + '}', featureProperties[key]);
            strokeWidth = strokeWidth.replace('${' + key + '}', featureProperties[key]);
            strokeDash = strokeDash.replace('${' + key + '}', featureProperties[key]);
            fillColor = fillColor.replace('${' + key + '}', featureProperties[key]);
            imageRadius = imageRadius.replace('${' + key + '}', featureProperties[key]);
            imageFillColor = imageFillColor.replace('${' + key + '}', featureProperties[key]);
            imageStrokeColor = imageStrokeColor.replace('${' + key + '}', featureProperties[key]);
            imageStrokeWidth = imageStrokeWidth.replace('${' + key + '}', featureProperties[key]);
            imageStrokeDash = imageStrokeDash.replace('${' + key + '}', featureProperties[key]);
          }
        }
      }

      //  This plugin assumes that all remaining html used to wrap the data fields has already been 
      //  stripped away in Views.  This is achieved in Views for each data field => field configuration
      //  => Style Settings => Customize field/label/wrapper HTML => "- None -".

      //  Final preparation of field content
      if (strokeColor == '') {
        strokeColor = undefined;
      } else {
        strokeColor = 'rgba(' + strokeColor + ')';
      }
      
      if (fillColor == '') {
        fillColor = undefined;
      } else {
        fillColor = 'rgba(' + fillColor + ')';
      }

      if (imageFillColor == '') {
        imageFillColor = undefined;
      } else {
        imageFillColor = 'rgba(' + imageFillColor + ')';
      }

      if (imageStrokeColor == '') {
        imageStrokeColor = undefined;
      } else {
        imageStrokeColor = 'rgba(' + imageStrokeColor + ')';
      }

      if (strokeDash == '' || strokeDash == '0,0') {
        strokeDash = undefined;
      } else {
        strokeDash = strokeDash.split(',');
      }

      if (imageStrokeDash == '' || imageStrokeDash == '0,0') {
        imageStrokeDash = undefined;
      } else {
        imageStrokeDash = imageStrokeDash.split(',');
      }

      return [
        new ol.style.Style({
          image: new ol.style.Circle({
            fill: new ol.style.Fill({
              color: imageFillColor
            }),
            stroke: new ol.style.Stroke({
              width: imageStrokeWidth,
              color: imageStrokeColor,
              lineDash: imageStrokeDash
            }),
            radius: imageRadius
          }),
          fill: new ol.style.Fill({
            color: fillColor
          }),
          stroke: new ol.style.Stroke({
            width: strokeWidth,
            color: strokeColor,
            lineDash: strokeDash
          })
        })
      ];
    };
  }
});
