Drupal.openlayers.pluginManager.register({
  fs: 'openlayers.Style:Icon',
  init: function(data) {
    return function (feature, resolution) {
      var srcContent = data.opt.path;
      var colorContent = data.opt.color || '';
      if (feature) {
        var featureProperties = feature.getProperties();
        for (key in featureProperties) {

          //  Replace tokens in src field
          if (key != 'popup_content' && key != 'tooltip_content') {
            oldSrcContent = srcContent + '.';
            while (oldSrcContent != srcContent) {
              oldSrcContent = srcContent;
              srcContent = srcContent.replace('${' + key + '}', featureProperties[key]);
            }
          }

          //  Replace tokens in color field
          if (key != 'popup_content' && key != 'tooltip_content') {
            oldColorContent = colorContent + '.';
            while (oldColorContent != colorContent) {
              oldColorContent = colorContent;
              colorContent = colorContent.replace('${' + key + '}', featureProperties[key]);
            }
          }
        }
      }  

      //  This plugin assumes that all remaining html used to wrap the data fields has already been 
      //  stripped away in Views.  This is achieved in Views for each data field => field configuration
      //  => Style Settings => Customize field/label/wrapper HTML => "- None -".

      //  Final preparation of field content
      if (srcContent == '') {
        srcContent = 'unknown';
      }

      if (colorContent == '') {
        colorContent = undefined;
      } else {
        colorContent = 'rgba(' + colorContent + ')';
      }
      
      return new ol.style.Style({
        image: new ol.style.Icon(({
          scale: data.opt.scale,
          anchor: data.opt.anchor,
          anchorXUnits: 'fraction',
          anchorYUnits: 'fraction',
          src: srcContent,
          color: colorContent,
        }))
      });
    };
  }
});
