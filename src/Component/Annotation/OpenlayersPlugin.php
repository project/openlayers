<?php

namespace Drupal\openlayers\Component\Annotation;

use OpenlayersDrupal\Component\Annotation\Plugin;

/**
 * Defines an Openlayers Plugin annotation object.
 *
 * @ingroup plugin_api
 *
 * @Annotation
 */
class OpenlayersPlugin extends Plugin {

}
