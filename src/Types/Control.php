<?php

namespace Drupal\openlayers\Types;

/**
 * FIX: Insert short comment here.
 */
abstract class Control extends Base implements ControlInterface {

  // Declare properties explicitly
  public $enabled;

  /**
   * The array containing the options.
   *
   * @var array
   */
  protected $options;

}
