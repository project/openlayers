<?php

namespace Drupal\openlayers\Types;

/**
 * FIX: Insert short comment here.
 */
abstract class Interaction extends Base implements InteractionInterface {

  // Declare properties explicitly
  public $enabled;

  /**
   * The array containing the options.
   *
   * @var array
   */
  protected $options;

}
