<?php

namespace Drupal\openlayers\KeyValueStore;

use OpenlayersDrupal\Core\KeyValueStore\KeyValueFactory as BaseKeyValueFactory;

/**
 * Overrides the core KV factory to use the 'service_container' container.
 *
 * @codeCoverageIgnore
 */
class KeyValueFactory extends BaseKeyValueFactory {

}
